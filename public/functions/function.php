<?php
	function get_header(){
		require_once("./public/includes/header.php");
	}
	function get_bread(){
		require_once("./public/includes/breadcumb.php");
	}
	function get_bread_two(){
		require_once("./public/includes/breadcumb_two.php");
	}
	function get_bread_three(){
		require_once("./public/includes/breadcumb_three.php");
	}
	function get_bread_four(){
		require_once("./public/includes/breadcumb_four.php");
	}
	function get_sidebar(){
		require_once("./public/includes/sidebar.php");
	}
	function get_part($addpart){
		require_once("./public/includes/".$addpart);
	}
 	function get_footer(){
		require_once("./public/includes/footer.php");
	}
?>
