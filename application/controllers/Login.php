<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class login extends CI_controller//controller class login
 {

    function __construct() {
        parent::__construct();
    }

    //-------this is a function for login-----//
    public function index() {
        if (empty($this->input->post())) {
//                        $this->load->view('header');
            $this->load->view('login/login');
            // $this->load->view('footer');
        } else {

            $this->form_validation->set_rules('userEmail', 'email', 'required|valid_email');
            $this->form_validation->set_rules('userPassword', 'password', 'required|min_length[8]|max_length[20]');

            if ($this->form_validation->run() == FALSE) {
                $error = validation_errors();
                print_r($error);
            } else {
                $user_data = array(
                    's_email' => $this->input->post('userEmail'),
                    's_password' => md5($this->input->post('userPassword')),
                );
                $userstatus = $this->Common_model->fetch_data('s_admin', '*', array('where' => array('a_email' => trim($user_data['s_email']), 'a_password' => trim($user_data['s_password']))), true);
                if ($userstatus['status'] == 1) {
//                print_r($userstatus); die;
                    $status['id'] = $userstatus['a_id'];
                    $this->session->set_userdata($status);
                    return redirect(base_url('index.php/Survey/'));
//                $this->load->view('admin/dashboard');
                    //redirect('404.php');
                } else {
                    
                }
            }
        }
    }

}
