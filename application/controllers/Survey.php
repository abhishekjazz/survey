<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Survey extends CI_controller  {

    function __construct() {
        parent::__construct();
    }

    public function index() {
        if ($this->session->userdata('id') == '') {
            return redirect(base_url('Login/'));
        } else {
            if ($this->session->userdata('id') != '') {
                $id = $this->session->userdata('id');
                $data['result'] = $this->Common_model->fetch_data('survey_des');

                $this->load->view('admin/dashboard');
                $this->load->view('admin/survey', $data);
            }
        }
    }

    /**
     * Description use to add surveys
     * @return void 
     *      
     */
    public function addSurvey() {
        if ($this->session->userdata('id') == '') {
            return redirect(base_url('Login/'));
        } else {
//            $this->load->view('admin/dashboard');
//            $this->load->view('admin/addsurvey');
            if ($this->input->get('title')) {
                $user_data = array(
                    's_title' => $this->input->get('title'),
                    's_description' => $this->input->get('desc'),
                    'status' => 0,
                    'created_time' => date('Y-m-d H:i:s')
                );

                $res = $this->Common_model->insert_single('survey_des', $user_data);
                if ($res) {
                    return redirect(base_url('index.php/Survey/'));
                }
            } else {
                $this->load->view('admin/dashboard');
                $this->load->view('admin/addsurvey');
            }
        }
    }

    /**
     * Description Function use to add question to each specific survey
     * @return void 
     */
    public function addquestion() {
        $a['id'] = $this->input->get('des_id');
//          echo $a;
        if ($this->session->userdata('id') == '') {
            return redirect(base_url('Login/'));
        } else {
//             echo $a;
            if ($this->input->post('ques')) {
                $aa = $this->input->post();
//                print_r($aa);die;
                $ques = array(
                    'q_question' => $this->input->post('ques'),
                    'q_answer' => $this->input->post('correct'),
                    'survey_id' => $this->input->post('survey_id'),
                    'o_option1' => $this->input->post('1'),
                    'o_option2' => $this->input->post('2'),
                    'o_option3' => $this->input->post('3'),
                    'o_option4' => $this->input->post('4'),
                    'q_status' => 1,
                    'created_date' => date('Y-m-d H:i:s')
//		'updated_time' => date('Y-m-d H:i:s')
                );
                $quesId = $this->Common_model->insert_single('s_question', $ques);
                if ($quesId) {
                    return redirect(base_url('index.php/Survey/'));
                }

//        print_r($d);die; 
            } else {
                $this->load->view('admin/dashboard');
                $this->load->view('admin/addQuestion', $a);
            }
        }
    }

    /**
     * Description Function use to LIST question to each specific survey
     * @return void 
     */
    public function questions() {
//        $data = array();
        $data['id'] = $this->input->get('des_id');
        if ($this->session->userdata('id') == '') {
            return redirect(base_url('Login/'));
        } else {
            $data['result'] = $this->Common_model->fetch_data('s_question', '*', array('where' => array('survey_id' => trim($data['id']))), true);
//            echo '<pre>';
//            print_r($data);die;

            $this->load->view('admin/dashboard');
            $this->load->view('admin/ques_list', $data);
        }
    }

    /**
     * Description Function use to Change status of question to each specific survey
     * @return void 
     */
    public function change_status() {
        $status = $this->input->post('q_status');
        $userid = $this->input->post('q_id');
        echo "$status" . "   dfhfgh   " . "$userid";
        $this->Common_model->update_status($userid, $status);
        //print_r($a);die;
    }

    public function logout() {
        if ($this->session->userdata('id') == '') {
            return redirect(base_url('Login/'));
        } else {
            $this->session->unset_userdata('id');
            $this->session->sess_destroy();
            redirect('index.php/Login/', 'refresh');
        }
    }

}
