
<!DOCTYPE html>
<html>
<head>
	<title>Login Page</title>
<!---link taken for the bootstrap stylesheet-->
<link id="base-style" href="<?php echo base_url()?>public/css/login.css" rel="stylesheet">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
<!--css stylesheet for this page-->
</head>

<body onload="myFunction()" style="margin:0;background:#3c8dbc;font-family: 'Open Sans', sans-serif;">
<div id="loader"></div>
<div class="container animate-bottom" style="margin-top: 1%; "  id="myDiv">

	  <ul class="nav nav-tabs">
	    <li class="active"><a data-toggle="tab" href="#home" class="view">LOGIN</a></li>
	   
	  </ul>

  <div class="tab-content">
    <div id="home" class="tab-pane fade in active">
		<div class="row row_login"><!---row starts here-->
			<div class="col-md-6 col-md-offset-3 inner_col"><!--column of colum of 6 and off set of 3-->
			<div class="form-group login_header">
				    <label for="Login" class="login">LOGIN</label><!--label showing for the login-->
				    <hr>
			</div>
				<?php echo form_open('Login'); ?><!--form starts here-->

				<div class="form-group">
				    <label for="email">Username:</label><!--label of username-->
				 	
					<input type="text" name="userEmail" autocomplete="off" placeholder="Enter your userEmail" class="type" value="<?php echo set_value('userEmail');?>"><!--input for username-->
					<span class="text-danger alert" style="margin-top: -17px;"><?php echo form_error('userEmail'); ?></span>
				</div>
				<div class="form-group">
				    <label for="password">Password:</label><!--label of password-->
				    <input type="password" name="userPassword" autocomplete="off" placeholder="Enter your password" class="type" value="<?php echo set_value('password');?>"><!--input for password-->
				 	<span class="text-danger alert" style="margin-top: -17px;"><?php echo form_error('password'); ?></span>
				</div>
				<div class="form-group">
					<input type="submit" value="Login" class="btn btn-info"><!--button for submit-->
				</div>

				  <?php
			      echo '<label class="text-danger">'.$this->session->flashdata("error").'</label>';
				   ?><!--message showing the error-->

				</form><!--form close here-->
				<div>Don't have an account?<a href="<?php echo base_url('Signup/index'); ?>">Sign up</a></div>
			</div>

</div>
<!---first tab closed-->

<!--second tab starts-->

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</body>
</html>
