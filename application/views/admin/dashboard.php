

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>AdminLTE 2 | Top Navigation</title>
        <base href="<?php echo base_url(); ?>">
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.7 -->
        <link rel="stylesheet" href="public/bower_components/bootstrap/dist/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="public/bower_components/font-awesome/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="public/bower_components/Ionicons/css/ionicons.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="public/css/AdminLTE.min.css">
        <link rel="stylesheet" href="public/css/searchcss.css">
        <!-- AdminLTE Skins. Choose a skin from the css/skins
             folder instead of downloading all of them to reduce the load. -->
        <link rel="stylesheet" href="public/css/skins/_all-skins.min.css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- Google Font -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    </head>
    <!-- ADD THE CLASS layout-top-nav TO REMOVE THE SIDEBAR. -->
    <body class="hold-transition skin-blue fixed sidebar-mini">
        <div class="wrapper">

            <header class="main-header">
                <!-- Logo -->
                <a href="http://localhost/sap_training/Users_Controller/User" class="logo">
                    <!-- mini logo for sidebar mini 50x50 pixels -->
                    <span class="logo-mini"><b>A</b>LT</span>
                    <!-- logo for regular state and mobile devices -->
                    <span class="logo-lg"><b>Admin</b>LTE</span>
                </a>
                <!-- Header Navbar: style can be found in header.less -->
                <nav class="navbar navbar-static-top">
                    <!-- Sidebar toggle button-->
                    <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </a>

                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">

                            <!-- User Account: style can be found in dropdown.less -->
                            <li class="dropdown user user-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <img src="public/img/user2-160x160.jpg" class="user-image" alt="User Image">
                                    <span class="hidden-xs">Alexander Pierce</span>
                                </a>
                                <ul class="dropdown-menu">
                                    <!-- User image -->
                                    <li class="user-header">
                                        <img src="public/img/user2-160x160.jpg" class="img-circle" alt="User Image">

                                        <p>
                                            <?php //print_r($info);die; ?>
                                            <!-- Alexander Pierce - Web Developer -->
                                            <small>Member since Nov. 2012</small>
                                        </p>
                                    </li>

                                    <!-- Menu Footer-->
                                    <li class="user-footer">
                                        <div class="pull-left">
                                            <a href="<?php echo base_url('Users/') ?>" class="btn btn-default btn-flat">Profile</a>
                                        </div>
                                        <div class="pull-right">
                                            <a href="http://localhost/survey1/index.php/Survey/logout" class="btn btn-default btn-flat">Sign out</a>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                            <!-- Control Sidebar Toggle Button -->
                            <!-- <li>
                              <a href="" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                            </li> -->
                        </ul>
                    </div>
                </nav>
            </header>

            <!-- =============================================== -->

            <!-- Left side column. contains the sidebar -->
            <aside class="main-sidebar">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image">
                            <img src="public/img/user2-160x160.jpg" class="img-circle" alt="User Image">
                        </div>
                        <div class="pull-left info">
                            <p>Alexander Pierce</p>
                            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                        </div>
                    </div>

                    <!-- sidebar menu: : style can be found in sidebar.less -->


                    <ul class="sidebar-menu" data-widget="tree">
                        <li class="header">MAIN NAVIGATION</li>
                        <li class="treeview">
                            <a href="http://localhost/sap_training/Users_Controller/index">
                                <i class="fa fa-dashboard"></i><span>Dashboard</span>
                                <!-- <span class="pull-right-container">
                                  <i class="fa fa-angle-left pull-right"></i>
                                </span> -->
                            </a>
                        <li><a href="http://localhost/survey/index.php/Survey/"><i class="icon-user "></i><span class="hidden-tablet">Survey</span></a></li>
                        <li><a href="http://localhost/survey/index.php/Survey/addSurvey"><i class="icon-user "></i><span class="hidden-tablet">Add Survey</span></a></li>
<!--                        <li><a href="http://localhost/survey/index.php/Survey/addquestion"><i class="icon-user "></i><span class="hidden-tablet">Add Question</span></a></li>-->
                                    <?php
                                    // {
                                    // $id=$rows['userID'];
                                    // }
//        if (!empty($role)) {
//
//          foreach ($role as $rows)
//          {
//           $role=$rows['roleID'];
//           if($role==17)
//           {
//              echo '<li><a href="http://localhost/sap_training/Users_Controller/User"><i class="icon-user"></i><span class="hidden-tablet">User</span></a></li>';
//           }
//             if($role==10)
//           {
//           	echo '<li><a href="javascript:void(0)"><i class="icon-user"></i><span class="hidden-tablet">create Request</span></a></li>';
//           }
//           if($role==11)
//           {
//            	echo '<li><a href="javascript:void(0)"><i class="icon-user"></i><span class="hidden-tablet">Approve Request</span></a></li>';
//           }
//           if($role==12)
//           {
//            	echo '<li><a href="javascript:void(0)"><i class="icon-user"></i><span class="hidden-tablet">Display</span></a></li>';
//           }
//            if($role==13)
//           {
//            	echo '<li><a href="javascript:void(0)"><i class="icon-user"></i><span class="hidden-tablet">Create Bid</span></a></li>';
//           }
//           if($role==14)
//           {
//            	echo '<li><a href="javascript:void(0)"><i class="icon-user"></i><span class="hidden-tablet">Create Option</span></a></li>';
//           }
//            if($role==15)
//           {
//            	echo '<li><a href="javascript:void(0)"><i class="icon-user"></i><span class="hidden-tablet">Go Live</span></a></li>';
//           }
//            if($role==16)
//           {
//            	echo '<li><a href="http://localhost/sap_training/Category/index"><i class="icon-user"></i><span class="hidden-tablet" >Catagory</span></a></li>';
//           }
//          }
//}
                                    ?>
                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>

            <!-- =============================================== -->

  <!-- <section class="content-header">
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Layout</a></li>
      <li class="active">Collapsed Sidebar</li>
    </ol>
  </section> -->

            <!-- Full Width Column -->
            <!-- /.content-wrapper -->
            <!-- <footer class="main-footer">
              <div class="container">
                <div class="pull-right hidden-xs">
                  <b>Version</b> 2.4.0
                </div>
                <strong>Copyright &copy; 2014-2016 <a href="https://adminlte.io">Almsaeed Studio</a>.</strong> All rights
                reserved.
              </div>
            <!-- /.container -->
            <!-- </footer>  -->
        </div>
        <!-- ./wrapper -->

        <!-- jQuery 3 -->
        <script src="public/bower_components/jquery/dist/jquery.min.js"></script>
        <!-- Bootstrap 3.3.7 -->
        <script src="public/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <!-- SlimScroll -->
        <script src="public/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
        <!-- FastClick -->
        <script src="public/bower_components/fastclick/lib/fastclick.js"></script>
        <!-- AdminLTE App -->
        <script src="public/js1/adminlte.min.js"></script>
        <!-- AdminLTE for demo purposes -->
        <script src="public/js1/demo.js"></script>
    </body>
</html>
