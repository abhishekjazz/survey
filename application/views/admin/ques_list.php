<?php // echo '<pre>'; echo $id; die;   ?>
<div class="content-wrapper">
    <div class="row">
        <div class="box span12" style="width:96%; margin-left:2%; margin-top:0.5%; ">
            <div class="box topnav " style="height:50px;">

                <!--<h2><i class="halflings-icon white user"></i><span class="break"></span>USERS</h2>-->
                <form action="index.php/Survey/addQuestion?des_id=<?php echo $id; ?>" name="addQues" method="POST">

                    <input type="submit" name="addques" class="btn btn-primary" value="ADD" style="float: right; margin-right: 50px; margin-top: 10px; "/>
                </form>
            </div>
            <div>
                <!--<input type="submit" name="addques" class="btn btn-primary" value="ADD" style="float: right;"/>-->
            </div>
            <div class="box-body table-responsive no-padding">

                <table class="table table-bordered table-hover">
                    <th>Index</th>
                    <th>Question</th>
                    <th>Choice 1</th>
                    <th>Choice 1</th>
                    <th>Choice 1</th>
                    <th>Choice 1</th>
                    <th>Answer</th>
                    <th>Active/Inactive</th>								
                    <tbody>
                        <?php
                        foreach ($result as $data) {
                            $serial = $this->uri->segment(5);
                            ?>
                            <tr>
                                <td><?php ++$serial ?></td>
    <!--                                <td><a href="<?php //echo base_url();  ?>index.php/Survey/addquestion?des_id=<?php echo $data['q_id']; ?>">
                                    </a></td> -->
                                <td><?php echo $data['q_question']; ?></a></td>
                                <td><?php echo $data['o_option1']; ?></td>
                                <td><?php echo $data['o_option2']; ?></td>
                                <td><?php echo $data['o_option3']; ?></td>
                                <td><?php echo $data['o_option4']; ?></td>
                                <td><?php echo $data['q_answer']; ?></td>
                                <td><i data="<?php echo ($data['q_status']) ?>" onclick='getConfirmation("<?php echo $data['q_id']; ?>", "<?php echo $data['q_status']; ?>")' class="status_checks btn btn-block btn-sm <?php echo ($data['q_status'] == 1) ? 'btn-success' : 'btn-danger' ?>"><?php echo ($data['q_status']) ? 'Active' : 'Inactive' ?>
                                    </i></td>
                            </tr>
                            <?php
                        } //close of foreach
//							
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    /*
     * @param current Userid is taken
     * @param current status is taken to Deactivate or Activate the user
     */
    /*
     * @param current Userid is taken
     * @param current status is taken to Deactivate or Activate the user
     */
    function getConfirmation(id, status) {
        var status = (status == 1) ? '0' : '1';
        var msg = (status == '0') ? 'Deactivate' : 'Activate';
        if (confirm("Are you sure to " + msg)) {
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>index.php/Survey/change_status",
                data: {"q_id": id, "q_status": status},
                success: function (data)
                {
                    location.reload();
                }
            });
        }
    }
</script>
